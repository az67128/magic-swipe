import ArcaneSurge from '$lib/spell/arcaneSurge.svelte';
import AbyssalEcho from '$lib/spell/abyssalEcho.svelte';
import MysticWard from '$lib/spell/mysticWard.svelte';
import IridescentShield from '$lib/spell/iridescentShield.svelte';
import DarkSphere from '$lib/spell/darkSphere.svelte';
import ManaRecovery from '$lib/spell/manaRecovery.svelte';
import RedSpehe from '$lib/spell/redSpehe.svelte';
import Heal from '$lib/spell/heal.svelte';
import ArcaneVortex from '$lib/spell/arcaneVortex.svelte';
import CelestialCall from '$lib/spell/celestialCall.svelte';
import TemporalVortex from '$lib/spell/temporalVortex.svelte';
import Stardust from '$lib/spell/stardust.svelte';
import LunarLullaby from '$lib/spell/lunarLullaby.svelte';
import SpectralVortex from '$lib/spell/spectralVortex.svelte';
import SerenityVeil from '$lib/spell/serenityVeil.svelte';
import AetherShards from '$lib/spell/aetherShards.svelte';
import StarfirePulse from '$lib/spell/starfirePulse.svelte';
import SpiritsEmbrace from '$lib/spell/spiritsEmbrace.svelte';
import TearRequiem from '$lib/spell/tearRequiem.svelte';
import FireBloom from '$lib/spell/fireBloom.svelte';

export default {
	ArcaneSurge,
	AbyssalEcho,
	MysticWard,
	IridescentShield,
	DarkSphere,
	ManaRecovery,
	RedSpehe,
	Heal,
	ArcaneVortex,
	CelestialCall,
	TemporalVortex,
	Stardust,
	LunarLullaby,
	SpectralVortex,
	SerenityVeil,
	AetherShards,
	StarfirePulse,
	SpiritsEmbrace,
	TearRequiem,
	FireBloom
};
