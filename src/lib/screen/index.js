import Battle from './battle/index.svelte';
import Win from './win/index.svelte';
import Lose from './lose/index.svelte';
import Skills from './skills/index.svelte';
import Tavern from './tavern/index.svelte';
import { SCREEN } from '$lib/store/constants.js';
export default {
	[SCREEN.BATTLE]: Battle,
    [SCREEN.WIN]: Win,
    [SCREEN.LOSE]: Lose,
    [SCREEN.SKILLS]: Skills,
    [SCREEN.TAVERN]: Tavern
};
