import { writable } from "svelte/store";
import { skills as initialSkills, gems as initialGems } from "./initialData";
// import { persisted } from 'svelte-local-storage-store'

const createSkillsStore = ()=>{
    const {set, update, subscribe} = writable(initialSkills);
    return{
        set, update, subscribe
    }
}
const createGemsStore = ()=>{
    const {set, update, subscribe} = writable(initialGems);
    return{
        set, update, subscribe
    }
}

export const skills = createSkillsStore();
export const gems = createGemsStore();
