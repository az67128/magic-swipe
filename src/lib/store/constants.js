export const CASTING_TIMEOUT = 100;
export const ANIMATION_TIME = 1500;
export const TURN_TIME = 2000;
export const CASTING_TIME = 1000;

export const SCREEN = {
    BATTLE: "BATTLE",
    WIN: "WIN",
    LOSE: "LOSE",
    SKILLS: "SKILLS",
    TAVERN: "TAVERN"
}