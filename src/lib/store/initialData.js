export let skills = {
	mysticWard: {id:'MysticWard', name: 'Mystic Ward', color: 'blue', taget: 'enemy', type: '', cost: 56, value: 39, icon: '/images/skills/mysticWard.webp', target: 'enemy', level: 5, gold: 40000},
	temporalVortex: {id:'TemporalVortex', name: 'Temporal Vortex', color: 'blue', taget: 'enemy', type: '', cost: 57, value: 38, icon: '/images/skills/temporalVortex.webp', target: 'enemy', level: 5, gold: 40000},
	spectralVortex: {id:'SpectralVortex', name: 'Spectral Vortex', color: 'blue', taget: 'enemy', type: '', cost: 55, value: 35, icon: '/images/skills/spectralVortex.webp', target: 'enemy', level: 4, gold: 8000},
	tearRequiem: {id:'TearRequiem', name: 'Tear Requiem', color: 'blue', taget: 'enemy', type: '', cost: 44, value: 30, icon: '/images/skills/tearRequiem.webp', target: 'enemy', level: 4, gold: 8000},
	arcaneVortex: {id:'ArcaneVortex', name: 'Arcane Vortex', color: 'purple', taget: 'enemy', type: '', cost: 38, value: 25, icon: '/images/skills/arcaneVortex.webp', target: 'enemy', level: 3, gold: 3000},
	arcaneSurge: {id:'ArcaneSurge', name: 'Arcane Surge', color: 'purple', taget: 'enemy', type: '', cost: 12, value: 10, icon: '/images/skills/arcaneSurge.webp', target: 'enemy', level: 1, gold: 0},
	iridescentShield: {id:'IridescentShield', name: 'Iridescent Shield', color: 'red', taget: 'enemy', type: '', cost: 44, value: 30, icon: '/images/skills/iridescentShield.webp', target: 'enemy', level: 4, gold: 8000},
	celestialCall: {id:'CelestialCall', name: 'Celestial Call', color: 'red', taget: 'enemy', type: '', cost: 32, value: 22, icon: '/images/skills/celestialCall.webp', target: 'enemy', level: 3, gold: 3000},
	stardust: {id:'Stardust', name: 'Stardust', color: 'red', taget: 'enemy', type: '', cost: 30, value: 20, icon: '/images/skills/stardust.webp', target: 'enemy', level: 3, gold: 3000},
	spiritsEmbrace: {id:'SpiritsEmbrace', name: 'Spirits Embrace', color: 'red', taget: 'enemy', type: '', cost: 28, value: 18, icon: '/images/skills/spiritsEmbrace.webp', target: 'enemy', level: 2, gold: 800},
	fireBloom: {id:'FireBloom', name: 'Fire Bloom', color: 'red', taget: 'enemy', type: '', cost: 23, value: 14, icon: '/images/skills/fireBloom.webp', target: 'enemy', level: 1, gold: 0},
	abyssalEcho: {id:'AbyssalEcho', name: 'Abyssal Echo', color: 'white', taget: 'enemy', type: '', cost: 40, value: 30, icon: '/images/skills/abyssalEcho.webp', target: 'enemy', level: 4, gold: 8000},
	darkSphere: {id:'DarkSphere', name: 'Dark Sphere', color: 'white', taget: 'enemy', type: '', cost: 55, value: 40, icon: '/images/skills/darkSphere.webp', target: 'enemy', level: 5, gold: 40000},
	lunarLullaby: {id:'LunarLullaby', name: 'Lunar Lullaby', color: 'white', taget: 'enemy', type: '', cost: 75, value: 50, icon: '/images/skills/lunarLullaby.webp', target: 'enemy', level: 5, gold: 40000},
	serenityVeil: {id:'SerenityVeil', name: 'Serenity Veil', color: 'white', taget: 'enemy', type: '', cost: 5, value: 5, icon: '/images/skills/serenityVeil.webp', target: 'enemy', level: 1, gold: 0},
	aetherShards: {id:'AetherShards', name: 'Aether Shards', color: 'white', taget: 'enemy', type: '', cost: 28, value: 18, icon: '/images/skills/aetherShards.webp', target: 'enemy', level: 2, gold: 500},
	starfirePulse: {id:'StarfirePulse', name: 'Starfire Pulse', color: 'white', taget: 'enemy', type: '', cost: 25, value: 15, icon: '/images/skills/starfirePulse.webp', target: 'enemy', level: 1, gold: 0},
	heal: {id:'Heal', name: 'Heal', color: 'green', taget: 'self', type: 'healthRecovery', cost: 10, value: 15, icon: '/images/skills/heal.webp', target: 'self', level: 1, gold: 0},
	manaRecovery: {id:'ManaRecovery', name: 'Mana Recovery', color: 'blue', taget: 'self', type: 'manaRecovery', cost: 0, value: 15, icon: '/images/skills/manaRecovery.webp', target: 'self', level: 1, gold: 0},
};

export let gems = {
	blue1: { id: 'blue1', image: '/images/crystals/blue.webp', effect: 'cost', value: -2, gold: 100, color: 'blue'},
blue2: { id: 'blue2', image: '/images/crystals/blue2.png', effect: 'value', value: 2, gold: 100, color: 'blue'},
blue3: { id: 'blue3', image: '/images/crystals/blue3.png', effect: 'value', value: 2, gold: 2000, color: 'blue'},
blue4: { id: 'blue4', image: '/images/crystals/blue4.png', effect: 'cost', value: -2, gold: 3000, color: 'blue'},
green1: { id: 'green1', image: '/images/crystals/green.webp', effect: 'cost', value: -2, gold: 50, color: 'green'},
green2: { id: 'green2', image: '/images/crystals/green2.png', effect: 'value', value: 2, gold: 1500, color: 'green'},
green3: { id: 'green3', image: '/images/crystals/green3.png', effect: 'value', value: 2, gold: 2000, color: 'green'},
purple1: { id: 'purple1', image: '/images/crystals/purple.webp', effect: 'cost', value: -2, gold: 1000, color: 'purple'},
purple2: { id: 'purple2', image: '/images/crystals/purple2.png', effect: 'value', value: 2, gold: 2000, color: 'purple'},
purple3: { id: 'purple3', image: '/images/crystals/purple3.png', effect: 'value', value: 2, gold: 3000, color: 'purple'},
red1: { id: 'red1', image: '/images/crystals/red.webp', effect: 'cost', value: -2, gold: 2000, color: 'red'},
red2: { id: 'red2', image: '/images/crystals/red2.png', effect: 'value', value: 2, gold: 2500, color: 'red'},
red3: { id: 'red3', image: '/images/crystals/red3.png', effect: 'value', value: 2, gold: 3000, color: 'red'},
white1: { id: 'white1', image: '/images/crystals/white.webp', effect: 'cost', value: -2, gold: 1000, color: 'white'},
white2: { id: 'white2', image: '/images/crystals/white2.png', effect: 'value', value: 2, gold: 2000, color: 'white'},
}

const skillArr = Object.keys(skills).slice(0, -1)
export const initialPlayerData = {
	level: 1,
	exp: 0,
	type: 'player',
	health: 100,
	mana: 100,
	name: 'Player',
	gold: 200,
	skills: {
		up: skills.starfirePulse,
		left: skills.serenityVeil,
		right: skills.heal,
		down: skills.manaRecovery
	},
	gems:[],
	avatar: `/images/avatar/${Math.floor(Math.random() * 138) + 1}.webp`,
	manaRecovery: 2
};


export const initialEnemyData = {
	type: 'enemy',
	health: 100,
	mana: 100,
	name: 'Enemy',
	gold: 100,
	gems:[],
	skills: {
		up: skills[skillArr[Math.floor(Math.random()*skillArr.length)]],
		left: skills[skillArr[Math.floor(Math.random()*skillArr.length)]],
		right: skills[skillArr[Math.floor(Math.random()*skillArr.length)]],
		down: skills.manaRecovery
	},
	avatar: `/images/avatar/${Math.floor(Math.random() * 138) + 1}.webp`,
	manaRecovery: 2,
};

export const initialBattleData = {
	turn: 'player',
	arena: `/images/taverns/${Math.floor(Math.random() * 4) + 1}.webp`
};
// window.skills = skills
