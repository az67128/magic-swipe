import { ANIMATION_TIME, CASTING_TIME, TURN_TIME, CASTING_TIMEOUT, SCREEN } from './constants.js';
import { writable, get, derived } from 'svelte/store';
import { initialEnemyData, initialPlayerData, initialBattleData, skills } from './initialData.js';

// import { persisted } from 'svelte-local-storage-store'

export const wait = async (time = 1000) => {
	return new Promise((resolve) => {
		setTimeout(resolve, time);
	});
};

const createScreenStore = () => {
	const { subscribe, set, update } = writable(SCREEN.TAVERN);
	return { subscribe, set, update };
};

const createBattleStore = () => {
	const { subscribe, set, update } = writable(initialBattleData);
	const lock = () => {
		update((state) => ({ ...state, isLocked: true }));
	};
	const passTurn = async () => {
		await wait(TURN_TIME);
		if (enemy.getMe().health <= 0) {
			screen.set(SCREEN.WIN);
			return;
		} else if (player.getMe().health <= 0) {
			screen.set(SCREEN.LOSE);
			return;
		}
		// enemy.regenerate();
		// player.regenerate();
		let newTurn;
		update((state) => {
			newTurn = state.turn === 'player' ? 'enemy' : 'player';
			return { ...state, turn: newTurn, isLocked: false };
		});
		if (newTurn === 'enemy') {
			enemy.enemyTurn();
		}
	};
	return { subscribe, set, update, passTurn, lock };
};

const createLogStore = () => {
	const { subscribe, set, update } = writable([]);
	let timeout;
	return {
		subscribe,
		set,
		update,
		log: async (text) => {
			clearTimeout(timeout);
			set([text]);
			timeout = setTimeout(() => set([]), ANIMATION_TIME);
		},
		add: async (text) => {
			update((state) => [state[0], text]);
		}
	};
};

const createCharacterStore = (data) => {
	const { subscribe, set, update } = writable(data);

	const getMe = () => {
		return get(data.type === 'player' ? player : enemy);
	};

	const getEffects = ()=>{
		return get(data.type === 'player' ? playerGems : enemyGems);
	}

	const getOpponent = () => {
		return data.type === 'player' ? enemy : player;
	};

	const getBattle = () => get(battle);

	const clearSpell = () => update((state) => ({ ...state, activeSpell: null }));

	const clearCasting = () => update((state) => ({ ...state, casting: false }));

	const gotDamage = (damage, spellId) => {
		animateSkill(spellId);
		let newHealth = 100;
		update((state) => {
			newHealth = state.health - damage <= 0 ? 0 : state.health - damage;
			return { ...state, health: newHealth };
		});
	};
	const regenerate = () => {
		update((state) => {
			const recoveredMana =
				state.mana + state.manaRecovery > 100 ? 100 : state.mana + state.manaRecovery;

			return { ...state, mana: recoveredMana };
		});
	};

	const animateSkill = async (spellId) => {
		update((state) => {
			return { ...state, activeSpell: spellId };
		});
		await wait(ANIMATION_TIME);
		clearSpell();
	};

	const useSkill = async (skill) => {
		const me = getMe();
		const effects = getEffects();
		if (getBattle().turn !== me.type || getBattle().isLocked) {
			return;
		}
		const targetSkill = me.skills[skill];
		const skillCost = targetSkill.cost + (effects[targetSkill.color]?.cost || 0) < 0 ? 0 : targetSkill.cost + (effects[targetSkill.color]?.cost || 0)
		const skillValue = targetSkill.value + (effects[targetSkill.color]?.value || 0 )
		if (me.mana < skillCost) {
			log.log('Not enough mana');
			return;
		}
		battle.lock();

		update((state) => ({ ...state, casting: targetSkill }));
		setTimeout(clearCasting, CASTING_TIME);

		log.log(targetSkill.name);
		await wait(CASTING_TIMEOUT);

		if (targetSkill.target === 'self') {
			animateSkill(targetSkill.id);
			if (targetSkill.type === 'manaRecovery') {
				update((state) => {
					const recoveredMana =
						state.mana + skillValue > 100 ? 100 : state.mana + skillValue;
					log.add(`Mana +${skillValue}`);
					return { ...state, mana: recoveredMana };
				});
			}
			if (targetSkill.type === 'healthRecovery') {
				update((state) => {
					const recoveredHealth =
						state.health + skillValue > 100 ? 100 : state.health + skillValue;
					log.add(`Health +${skillValue}`);
					return { ...state, health: recoveredHealth };
				});
			}
		} else {
			log.add(`Damage -${skillValue}`);
			getOpponent().gotDamage(skillValue, targetSkill.id);
		}

		update((state) => {
			return { ...state, mana: state.mana - skillCost };
		});
		battle.passTurn();
	};
	const enemyTurn = () => {
		const skills = ['up', 'left', 'right'];
		const me = getMe();
		let targetSkill = skills[Math.floor(Math.random() * skills.length)];
		if (me.skills[targetSkill].cost > me.mana) {
			targetSkill = 'down';
		}
		useSkill(targetSkill);
	};

	return {
		getMe,
		subscribe,
		set,
		update,
		useSkill,
		gotDamage,
		enemyTurn,
		regenerate
	};
};


const derivedGems = (character)=>{
	
	return derived(character, ($character) => {
		const effects = {}
		$character.gems.forEach(item=>{
			if(!effects[item.color]){
				effects[item.color] = {value: 0, cost: 0}
			}
			effects[item.color][item.effect] += item.value
		})
		return effects
	});
}

export const battle = createBattleStore();
export const log = createLogStore();
export const player = createCharacterStore(initialPlayerData);
export const enemy = createCharacterStore(initialEnemyData);
export const screen = createScreenStore();

export const playerGems = derivedGems(player)
export const enemyGems = derivedGems(enemy)


export const nextEmeny = async (enemyData = {}) => {
	screen.set(SCREEN.BATTLE);
	await wait(500);
	const skillArr = Object.keys(skills).slice(0, -1);
	
	enemy.update((state) => ({
		...state,
		health: 100,
		mana: 100,
		name: 'Enemy',
		avatar: `/images/avatar/${Math.floor(Math.random() * 133) + 1}.webp`,
		skills: {
			up: skills[skillArr[Math.floor(Math.random() * skillArr.length)]],
			left: skills[skillArr[Math.floor(Math.random() * skillArr.length)]],
			right: skills[skillArr[Math.floor(Math.random() * skillArr.length)]],
			down: skills.manaRecovery
		},
		...enemyData
	}));

	player.update((state) => ({
		...state,
		health: 100,
		mana: 100
	}));
	battle.update((state) => ({
		...state,
		isLocked: false,
		turn: 'player',
		arena: `/images/battlefield/${Math.floor(Math.random() * 28) + 1}.webp`
	}));
};
export const replay = () => {
	enemy.update((state) => ({
		...state,
		health: 100,
		mana: 100
	}));
	player.update((state) => ({
		...state,
		health: 100,
		mana: 100
	}));
	battle.update((state) => ({ ...state, isLocked: false, turn: 'player' }));
	screen.set(SCREEN.BATTLE);
};

export const toTavern = async () => {
	screen.set(SCREEN.TAVERN)
	await wait(300)
	battle.update((state) => ({ ...state, arena: `/images/taverns/${Math.floor(Math.random() * 4) + 1}.webp` }));
	
};